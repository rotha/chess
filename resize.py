import os
from PIL import Image
input_dir="pictures/tr"
output_dir="pictures"
def resize(size,to_print=False):
    for filename in os. listdir(input_dir):
        image = Image.open(input_dir+"/"+filename)
        image = image.resize((size, size))
        if to_print:
            print(f"file names:{input_dir}/{filename}--->{output_dir}/{filename[0:-7]}.png")
        image.save(f"{output_dir}/{filename[0:-7]}.png")