import pygame
import stockfish2
import chess
import resize

# configs--------------
rgb_light_tiles = (255, 214, 153)
rgb_dark_tiles = (188, 108, 37)
rgb_promote_tiles = (0, 25, 186)
rgb_highlight_tile = (242, 234, 2)
rgb_check_tile = (242, 48, 48)
# rgb_possible_moves = (104, 105, 110)
rgb_possible_moves = (7, 21, 92)
rgb_background = (0, 82, 24)

tile_length = 60

border_tiles = 1
number_of_tiles_long = 10
tile_length_to_circumference = 5
player_to_bw = {0: "white", 1: "black"}
# K for king, Q for queen, R for rook, B for bishop, and N for knight
one_letter_to_piece = {"k": "king", "q": "queen", "r": "rook", "b": "bishop", "n": "knight", "p": "pawn"}
one_letter_to_icon = {"k": "black_king", "q": "black_queen", "r": "black_rook", "b": "black_bishop",
                      "n": "black_knight", "p": "black_pawn",
                      "K": "white_king", "Q": "white_queen", "R": "white_rook", "B": "white_bishop",
                      "N": "white_knight", "P": "white_pawn"}
number_to_piece = {0: "Q", 1: "R", 2: "B", 3: "N"}

better_selection = True
ai_player = [1]


# -----------------------
# functions--------------

def draw_tile(i, j, colour):
    y = (i + border_tiles) * tile_length
    x = (j + border_tiles) * tile_length
    pygame.draw.rect(game_display, colour, (x, y, tile_length, tile_length))


def draw_light_tile(i, j):
    draw_tile(i, j, rgb_light_tiles)


def draw_dark_tile(i, j):
    draw_tile(i, j, rgb_dark_tiles)


def draw_board():
    for i in range(8):
        for j in range(8):
            if (i + j) % 2 == 0:
                draw_light_tile(i, j)
            else:
                draw_dark_tile(i, j)


def within_boarders(x, y):
    in_x_range = border_tiles * tile_length < x < border_tiles * tile_length + 8 * tile_length
    in_y_range = border_tiles * tile_length < y < border_tiles * tile_length + 8 * tile_length
    return in_x_range and in_y_range


def get_tile_from_click(x, y):
    if not within_boarders(x, y):
        return None
    x = (x - (border_tiles * tile_length)) / tile_length
    y = (y - (border_tiles * tile_length)) / tile_length
    j = int(str(x)[0:1])
    i = int(str(y)[0:1])
    return i, j


def get_xy_from_tile(i, j):
    y = (i + border_tiles) * tile_length
    x = (j + border_tiles) * tile_length
    return x, y


def get_xy_from_tile(position):
    y = (position[0] + border_tiles) * tile_length
    x = (position[1] + border_tiles) * tile_length
    return x, y


def draw_piece_old(player, piece, i, j):
    filename = f"pictures/{player_to_bw[player]}_{one_letter_to_piece[piece]}.png"
    img = pygame.image.load(filename)
    game_display.blit(img, get_xy_from_tile(i, j))


def delete_piece(tuple):
    i, j = tuple
    if (i + j) % 2 == 0:
        draw_light_tile(i, j)
    else:
        draw_dark_tile(i, j)


def draw_piece(piece, position):
    if (position[0] + position[1]) % 2 == 0:
        draw_light_tile(position[0], position[1])
    else:
        draw_dark_tile(position[0], position[1])
    draw_only_piece(piece, position)


def draw_only_piece(piece, position):
    if not piece == "":
        filename = f"pictures/{one_letter_to_icon[piece]}.png"
        img = pygame.image.load(filename)
        game_display.blit(img, get_xy_from_tile(position))


def draw_position(board):
    for index, piece in enumerate(board[0:chess.LAST_TILE_INDEX + 1]):
        draw_piece(piece, chess.index_to_tuple(index))
    draw_check_tile(board)


def draw_possible_move(tuple_to):
    x, y = get_xy_from_tile(tuple_to)
    x += tile_length / 2
    y += tile_length / 2

    radius = (tile_length / tile_length_to_circumference) / 2
    pygame.draw.circle(game_display, rgb_possible_moves, (x, y), radius, 0)


def reset_clicks():
    global first_click, second_click
    first_click, second_click = None, None


def highlight_tile(tupel, board):
    colour_tile_with_piece(tupel, board, rgb_highlight_tile)


def draw_check_tile(board):
    player = board[chess.PLAYER_INDEX]
    king_index = chess.find_king(board, player)
    if chess.tile_in_check(board, king_index, player):
        colour_tile_with_piece(chess.index_to_tuple(king_index), board, rgb_check_tile)


def colour_tile_with_piece(tupel, board, colour):
    i, j = tupel
    draw_tile(i, j, colour)
    piece = board[chess.tuple_to_index(tupel)]
    draw_only_piece(piece, tupel)


def draw_possible_moves(current_click):
    possible_moves = chess.get_possible_moves_for_display(current_click, chess_position)

    for move in possible_moves:
        draw_possible_move(move)


def select_square(new_click):
    global first_click, second_click, chess_position

    index = chess.tuple_to_index(new_click)
    player = chess.get_current_player(chess_position)
    if chess.piece_to_player(chess_position[index], player):
        first_click = new_click
        highlight_tile(first_click, chess_position)
        draw_possible_moves(new_click)
    else:
        draw_position(chess_position)


def add_click_and_move(new_click):
    global first_click, second_click, chess_position
    if first_click is None:
        select_square(new_click)
    else:
        index = chess.tuple_to_index(new_click)
        player = chess.get_current_player(chess_position)
        if better_selection and chess.piece_to_player(chess_position[index], player):
            draw_position(chess_position)
            select_square(new_click)
        else:
            chess_position = chess.move(first_click, new_click, chess_position)
            draw_position(chess_position)
            reset_clicks()


def promote_menu(player):
    for i in range(4):
        y = (i + 2 + border_tiles) * tile_length
        x = 0

        pygame.draw.rect(game_display, rgb_promote_tiles, (x, y, tile_length, tile_length))
        piece = number_to_piece[i]
        if player == 1:
            piece = piece.lower()

        filename = f"pictures/{one_letter_to_icon[piece]}.png"
        img = pygame.image.load(filename)
        game_display.blit(img, (x, y))


def close_promote_menu():
    for i in range(4):
        y = (i + 2 + border_tiles) * tile_length
        x = 0

        pygame.draw.rect(game_display, rgb_background, (x, y, tile_length, tile_length))


def get_click_from_promotion_menu(x, y):
    y = (y - (border_tiles * tile_length)) / tile_length
    i = int(str(y)[0:1]) - 2
    if 0 <= i <= 3 and x <= tile_length:
        return i


# initiate board
pygame.init()
game_display = pygame.display.set_mode((number_of_tiles_long * tile_length, number_of_tiles_long * tile_length))
game_display.fill(rgb_background)
resize.resize(tile_length)
draw_board()
# fen = "r1b1kbnr/p1p2p1p/6p1/Pp1q3P/2n2Q1R/8/1PPN1PP1/R1B1KBN1 w Qq - 0 1"
#fen = "8/8/2k5/1pp5/8/8/P7/4K3 w - - 0 1"
#fen = "5rk1/p2Q1ppp/4p3/3p4/4q1P1/6P1/P1r4P/1R3RK1 w - - 0 1"
#chess_position = chess.convert_fen_to_board(fen)
chess_position = chess.STARTING_BOARD
first_click = None
second_click = None
promoting = False
piece_to_promote = None
promoting_player = None

draw_position(chess_position)
# init ai
ai = stockfish2.Stockfish2()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        if chess_position[chess.PLAYER_INDEX] in ai_player:
            print("\rai calculating move...",end="")
            ai_move = ai.make_next_move(chess_position)
            print("\rdone!",end="")
            if ai_move is not None:
                chess_position = ai_move[3]
            draw_position(chess_position)
        if promoting:
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                click = get_click_from_promotion_menu(x, y)
                if not click is None:

                    piece = number_to_piece[click]
                    if promoting_player == 1:
                        piece = piece.lower()
                    chess.promote_pawn(piece_to_promote, piece, chess_position)
                    close_promote_menu()
                    promoting = False
                    piece_to_promote = None
                    promoting_player = None
                    draw_position(chess_position)
        else:
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                if within_boarders(x, y):
                    # print(f"click on {x},{y}: within borders->{get_tile_from_click(x, y)}")
                    i, j = get_tile_from_click(x, y)
                    # print(chess.tile_in_check(chess_position, chess.tuple_to_index((i, j)), 0))
                    add_click_and_move((i, j))
                    piece_to_promote = chess.check_for_pawn_promotion(chess_position)
                    if piece_to_promote is not None:
                        player = chess_position[chess.PLAYER_INDEX]
                        if player == 0:
                            player = 1
                        else:
                            player = 0
                        promote_menu(player)
                        promoting = True
                        promoting_player = player

    pygame.display.update()
