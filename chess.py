# constants
STARTING_BOARD = ["r", "n", "b", "q", "k", "b", "n", "r",
                  "p", "p", "p", "p", "p", "p", "p", "p",
                  "", "", "", "", "", "", "", "",
                  "", "", "", "", "", "", "", "",
                  "", "", "", "", "", "", "", "",
                  "", "", "", "", "", "", "", "",
                  "P", "P", "P", "P", "P", "P", "P", "P",
                  "R", "N", "B", "Q", "K", "B", "N", "R",
                  0, "KQkq", "-", 0, 1]

ALGEBRAIC_CHAR_TO_INT = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7}

PIECE_VALUES = {"": 0, "r": -5, "n": -3, "b": -3, "q": -9, "k": 0, "p": -1, "R": 5, "N": 3, "B": 3, "Q": 9, "K": 0,
                "P": 1}

LAST_TILE_INDEX = 63
PLAYER_INDEX = 64
CASTLE_INDEX = 65
ENPASSANT_INDEX = 66
HALFMOVE_CLOCK_INDEX = 67
NUMBER_OF_MOVES_INDEX = 68

UPWARD_INCREMENT = (-1, 0)
DOWNWARD_INCREMENT = (1, 0)
LEFT_INCREMENT = (0, -1)
RIGHT_INCREMENT = (0, 1)
UP_LEFT_INCREMENT = (-1, -1)
UP_RIGHT_INCREMENT = (-1, 1)
DOWN_RIGHT_INCREMENT = (1, 1)
DOWN_LEFT_INCREMENT = (1, -1)

KNIGHT_INCREMENTS = [(2, 1), (2, -1), (-2, 1), (-2, -1), (1, 2), (1, -2), (-1, 2), (-1, -2)]

WHITE_PIECES_PROMOTION = ["R", "Q", "B", "N", "P"]
DANGEROUS_WHITE_CLOSE_PIECES_STRAIGHT = ["K"]
DANGEROUS_WHITE_PIECES_STRAIGHT = ["R", "Q"]
DANGEROUS_WHITE_CLOSE_PIECES_DIAGONAL_UPWARDS = ["K"]
DANGEROUS_WHITE_CLOSE_PIECES_DIAGONAL_DOWNWARDS = ["K", "P"]
DANGEROUS_WHITE_PIECES_DIAGONAL = ["B", "Q"]

BLACK_PIECES_PROMOTION = ["r", "q", "b", "n", "p"]
DANGEROUS_BLACK_CLOSE_PIECES_STRAIGHT = ["k"]
DANGEROUS_BLACK_PIECES_STRAIGHT = ["r", "q"]
DANGEROUS_BLACK_CLOSE_PIECES_DIAGONAL_UPWARDS = ["k", "p"]
DANGEROUS_BLACK_CLOSE_PIECES_DIAGONAL_DOWNWARDS = ["k"]
DANGEROUS_BLACK_PIECES_DIAGONAL = ["b", "q"]


def get_current_player(board):
    return board[PLAYER_INDEX]


def get_other_player(player):
    return (player + 1) % 2


def add_tuples(tuple_one, tuple_two):
    return tuple_one[0] + tuple_two[0], tuple_one[1] + tuple_two[1]


def tuple_to_index(tuple):
    i = tuple[0]
    j = tuple[1]
    return (i * 8) + j


def index_to_tuple(index):
    return divmod(index, 8)


def algebraic_to_tupel(algebraic):
    return 7 - (int(algebraic[1]) - 1), ALGEBRAIC_CHAR_TO_INT[algebraic[0]]


def algebraic_to_index(algebraic):
    return tuple_to_index(algebraic_to_tupel(algebraic))


def convert_fen_to_board(fen):
    board = []
    space_counter = 0
    castles = ""
    enpassant = ""
    number_of_moves = ""
    halfmove_clock = ""
    for char in fen:
        if char == " ":
            space_counter += 1
        elif space_counter == 0:
            if not char == "/":
                if char.isdigit():
                    x = int(char)
                    for i in range(x):
                        board.append("")
                else:
                    board.append(char)

        elif space_counter == 1:
            if char == "w":
                board.append(0)
            else:
                board.append(1)

        elif space_counter == 2:
            castles += char

        elif space_counter == 3:
            enpassant += char

        elif space_counter == 4:
            halfmove_clock += char

        elif space_counter == 5:
            number_of_moves += char

    if castles == "-":
        board.append("")
    else:
        board.append(castles)

    if enpassant == "-":
        board.append(enpassant)
    else:
        board.append(algebraic_to_index(enpassant))
    board.append(int(halfmove_clock))
    board.append(int(number_of_moves))
    return board


def find_king(board, player):
    target_piece = "K"
    if player == 1:
        target_piece = "k"

    for index, tile in enumerate(board[:LAST_TILE_INDEX + 1]):
        if tile == target_piece:
            return index


def index_on_board(index):
    return LAST_TILE_INDEX >= index >= 0


def tuple_on_board(tuple):
    return 0 <= tuple[0] <= 7 and 0 <= tuple[1] <= 7


def tile_in_check_directional(board, position, increment, close_dangerous_pieces, dangerous_pieces):
    # first the square, directly next to it
    m = (position[0] + increment[0], position[1] + increment[1])
    if tuple_on_board(m):
        piece_found = board[tuple_to_index(m)]
        if not piece_found == "":
            return piece_found in close_dangerous_pieces or piece_found in dangerous_pieces
        else:
            # if there is no piece on the first square, then the next ones
            m = (m[0] + increment[0], m[1] + increment[1])
            while tuple_on_board(m):
                piece_found = board[tuple_to_index(m)]
                if not piece_found == "":
                    return piece_found in dangerous_pieces
                m = (m[0] + increment[0], m[1] + increment[1])
    return False


def tile_in_check_knight(board, position, player):
    dangerous_knight = "N"
    if player == 0:
        dangerous_knight = "n"

    for increment in KNIGHT_INCREMENTS:
        m = (position[0] + increment[0], position[1] + increment[1])
        if tuple_on_board(m) and board[tuple_to_index(m)] == dangerous_knight:
            return True
    return False


def tile_in_check(board, index, player):
    position = index_to_tuple(index)

    if player == 0:
        # check upwards
        if tile_in_check_directional(board, position, UPWARD_INCREMENT, DANGEROUS_BLACK_CLOSE_PIECES_STRAIGHT,
                                     DANGEROUS_BLACK_PIECES_STRAIGHT):
            return True
        # check downwards
        elif tile_in_check_directional(board, position, DOWNWARD_INCREMENT, DANGEROUS_BLACK_CLOSE_PIECES_STRAIGHT,
                                       DANGEROUS_BLACK_PIECES_STRAIGHT):
            return True
        # check left
        elif tile_in_check_directional(board, position, LEFT_INCREMENT, DANGEROUS_BLACK_CLOSE_PIECES_STRAIGHT,
                                       DANGEROUS_BLACK_PIECES_STRAIGHT):
            return True
        # check right
        elif tile_in_check_directional(board, position, RIGHT_INCREMENT, DANGEROUS_BLACK_CLOSE_PIECES_STRAIGHT,
                                       DANGEROUS_BLACK_PIECES_STRAIGHT):
            return True
        # check upwards left
        elif tile_in_check_directional(board, position, UP_LEFT_INCREMENT,
                                       DANGEROUS_BLACK_CLOSE_PIECES_DIAGONAL_UPWARDS,
                                       DANGEROUS_BLACK_PIECES_DIAGONAL):
            return True
        # check upwards right
        elif tile_in_check_directional(board, position, UP_RIGHT_INCREMENT,
                                       DANGEROUS_BLACK_CLOSE_PIECES_DIAGONAL_UPWARDS,
                                       DANGEROUS_BLACK_PIECES_DIAGONAL):
            return True
        # check downwards left
        elif tile_in_check_directional(board, position, DOWN_LEFT_INCREMENT,
                                       DANGEROUS_BLACK_CLOSE_PIECES_DIAGONAL_DOWNWARDS,
                                       DANGEROUS_BLACK_PIECES_DIAGONAL):
            return True
        # check downwards right
        elif tile_in_check_directional(board, position, DOWN_RIGHT_INCREMENT,
                                       DANGEROUS_BLACK_CLOSE_PIECES_DIAGONAL_DOWNWARDS,
                                       DANGEROUS_BLACK_PIECES_DIAGONAL):
            return True
        # check horseys
        else:
            return tile_in_check_knight(board, position, 0)
    else:
        if tile_in_check_directional(board, position, UPWARD_INCREMENT, DANGEROUS_WHITE_CLOSE_PIECES_STRAIGHT,
                                     DANGEROUS_WHITE_PIECES_STRAIGHT):
            return True
        # check downwards
        elif tile_in_check_directional(board, position, DOWNWARD_INCREMENT, DANGEROUS_WHITE_CLOSE_PIECES_STRAIGHT,
                                       DANGEROUS_WHITE_PIECES_STRAIGHT):
            return True
        # check left
        elif tile_in_check_directional(board, position, LEFT_INCREMENT, DANGEROUS_WHITE_CLOSE_PIECES_STRAIGHT,
                                       DANGEROUS_WHITE_PIECES_STRAIGHT):
            return True
        # check right
        elif tile_in_check_directional(board, position, RIGHT_INCREMENT, DANGEROUS_WHITE_CLOSE_PIECES_STRAIGHT,
                                       DANGEROUS_WHITE_PIECES_STRAIGHT):
            return True
        # check upwards left
        elif tile_in_check_directional(board, position, UP_LEFT_INCREMENT,
                                       DANGEROUS_WHITE_CLOSE_PIECES_DIAGONAL_UPWARDS,
                                       DANGEROUS_WHITE_PIECES_DIAGONAL):
            return True
        # check upwards right
        elif tile_in_check_directional(board, position, UP_RIGHT_INCREMENT,
                                       DANGEROUS_WHITE_CLOSE_PIECES_DIAGONAL_UPWARDS,
                                       DANGEROUS_WHITE_PIECES_DIAGONAL):
            return True
        # check downwards left
        elif tile_in_check_directional(board, position, DOWN_LEFT_INCREMENT,
                                       DANGEROUS_WHITE_CLOSE_PIECES_DIAGONAL_DOWNWARDS,
                                       DANGEROUS_WHITE_PIECES_DIAGONAL):
            return True
        # check downwards right
        elif tile_in_check_directional(board, position, DOWN_RIGHT_INCREMENT,
                                       DANGEROUS_WHITE_CLOSE_PIECES_DIAGONAL_DOWNWARDS,
                                       DANGEROUS_WHITE_PIECES_DIAGONAL):
            return True
        # check horseys
        else:
            return tile_in_check_knight(board, position, 1)


def check(board, player):
    king_index = find_king(board, player)
    return tile_in_check(board, king_index, player)


def change_player(board):
    if board[PLAYER_INDEX] == 0:
        board[PLAYER_INDEX] = 1
    else:
        board[PLAYER_INDEX] = 0

    return board


def piece_to_player(piece, player):
    piece_colour = 1
    if piece == "":
        return False
    if piece.isupper():
        piece_colour = 0

    return piece_colour == player


def check_for_pawn_promotion(board):
    for i in range(8):
        if board[i] == "P":
            return i

    for i in range(56, 64):
        if board[i] == "p":
            return i
    return None


def promote_pawn(index, piece, board):
    board[index] = piece


def check_on_multiple_squares(squares, player, board):
    for square in squares:
        if tile_in_check(board, square, player):
            return False
    return True


def check_if_line_is_free(tuple_from, tuple_to, i_increment, j_increment, player, board):
    i, j = tuple_from
    target_i, target_j = tuple_to

    i += i_increment
    j += j_increment

    while not i == target_i or not j == target_j:
        if not board[tuple_to_index((i, j))] == "":
            return False
        i += i_increment
        j += j_increment

    target_piece = board[tuple_to_index(tuple_to)]
    return not piece_to_player(target_piece, player)


def move_piece_without_checking_target(board, tuple_from, tuple_to, piece):
    new_board = list.copy(board)
    # make changes
    new_board[tuple_to_index(tuple_from)] = ""
    new_board[tuple_to_index(tuple_to)] = piece
    new_board[ENPASSANT_INDEX] = "-"
    return new_board


def move_oblique(tuple_from, tuple_to, player, board, piece_to_move):
    i1, j1 = tuple_from
    i2, j2 = tuple_to

    i_increment = 1
    if i1 > i2:
        i_increment = -1

    j_increment = 1
    if j1 > j2:
        j_increment = -1

    if check_if_line_is_free(tuple_from, tuple_to, i_increment, j_increment, player, board):
        return move_piece_without_checking_target(board, tuple_from, tuple_to, piece_to_move)


def move_horizontal_or_vertical(tuple_from, tuple_to, player, board, piece_to_move):
    i1, j1 = tuple_from
    i2, j2 = tuple_to

    i_increment = 0
    j_increment = 0
    if not i1 == i2:
        i_increment = 1
        if i1 > i2:
            i_increment = -1
    else:
        j_increment = 1
        if j1 > j2:
            j_increment = -1

    if check_if_line_is_free(tuple_from, tuple_to, i_increment, j_increment, player, board):
        return move_piece_without_checking_target(board, tuple_from, tuple_to, piece_to_move)


def move_piece(board, tuple_from, tuple_to, piece, player):
    poss_new_position = tuple_to_index(tuple_to)

    if piece_to_player(board[poss_new_position], player):
        return None

    new_board = list.copy(board)

    # make changes
    new_board[tuple_to_index(tuple_from)] = ""
    new_board[tuple_to_index(tuple_to)] = piece
    new_board[ENPASSANT_INDEX] = "-"
    return new_board


def move_pawn(tuple_from, tuple_to, player, board):
    if player == 0:
        # check square in front of pawn
        if tuple_to == (tuple_from[0] - 1, tuple_from[1]):
            poss_new_position = tuple_to_index(tuple_to)
            if board[poss_new_position] == "":
                return move_piece_without_checking_target(board, tuple_from, tuple_to, "P")
        # check if pawn can move 2 forward
        if tuple_to == (tuple_from[0] - 2, tuple_from[1]):
            poss_skipped_index = tuple_to_index((tuple_from[0] - 1, tuple_from[1]))
            poss_new_position = tuple_to_index(tuple_to)
            if tuple_from[0] == 6 and board[poss_new_position] == "" and board[poss_skipped_index] == "":
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "P")
                # r/anarchychess
                new_board[ENPASSANT_INDEX] = tuple_to_index((tuple_from[0] - 1, tuple_from[1]))
                return new_board

        # check infront to the left and right
        if tuple_to == (tuple_from[0] - 1, tuple_from[1] - 1) or tuple_to == (tuple_from[0] - 1, tuple_from[1] + 1):
            poss_new_position = tuple_to_index(tuple_to)
            if piece_to_player(board[poss_new_position], 1):
                return move_piece_without_checking_target(board, tuple_from, tuple_to, "P")
            if board[ENPASSANT_INDEX] == tuple_to_index(tuple_to):
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "P")
                new_board[tuple_to_index((3, tuple_to[1]))] = ""
                return new_board
    else:
        # check square in front of pawn
        if tuple_to == (tuple_from[0] + 1, tuple_from[1]):
            poss_new_position = tuple_to_index(tuple_to)
            if board[poss_new_position] == "":
                return move_piece_without_checking_target(board, tuple_from, tuple_to, "p")
        # check if pawn can move 2 forward
        if tuple_to == (tuple_from[0] + 2, tuple_from[1]):
            poss_skipped_index = tuple_to_index((tuple_from[0] + 1, tuple_from[1]))
            poss_new_position = tuple_to_index(tuple_to)
            if tuple_from[0] == 1 and board[poss_new_position] == "" and board[poss_skipped_index] == "":
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "p")
                # r/anarchychess
                new_board[ENPASSANT_INDEX] = tuple_to_index((tuple_from[0] + 1, tuple_from[1]))
                return new_board

        # check infront to the left and right
        if tuple_to == (tuple_from[0] + 1, tuple_from[1] - 1) or tuple_to == (tuple_from[0] + 1, tuple_from[1] + 1):
            poss_new_position = tuple_to_index(tuple_to)
            if piece_to_player(board[poss_new_position], 0):
                return move_piece_without_checking_target(board, tuple_from, tuple_to, "p")
            if board[ENPASSANT_INDEX] == tuple_to_index(tuple_to):
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "p")
                new_board[tuple_to_index((4, tuple_to[1]))] = ""
                return new_board


def move_knight(tuple_from, tuple_to, player, board):
    i1, j1 = tuple_from
    i2, j2 = tuple_to

    piece_to_move = "N"
    if player == 1:
        piece_to_move = "n"

    if abs(i1 - i2) == 2:
        if abs(j1 - j2) == 1:
            return move_piece(board, tuple_from, tuple_to, piece_to_move, player)

    if abs(i1 - i2) == 1 and abs(j1 - j2) == 2:
        return move_piece(board, tuple_from, tuple_to, piece_to_move, player)

    return None


def move_bishop(tuple_from, tuple_to, player, board):
    i1, j1 = tuple_from
    i2, j2 = tuple_to

    piece_to_move = "B"
    if player == 1:
        piece_to_move = "b"

    if abs(i1 - i2) == abs(j1 - j2):
        return move_oblique(tuple_from, tuple_to, player, board, piece_to_move)

    return None


def move_queen(tuple_from, tuple_to, player, board):
    i1, j1 = tuple_from
    i2, j2 = tuple_to

    piece_to_move = "Q"
    if player == 1:
        piece_to_move = "q"

    if abs(i1 - i2) == abs(j1 - j2):
        return move_oblique(tuple_from, tuple_to, player, board, piece_to_move)
    elif not (i1 == i2) == (j1 == j2):
        return move_horizontal_or_vertical(tuple_from, tuple_to, player, board, piece_to_move)


def move_king(tuple_from, tuple_to, player, board):
    i, j = tuple_from
    target_i, target_j = tuple_to

    piece_to_move = "K"
    if player == 1:
        piece_to_move = "k"

    if abs(i - target_i) <= 1 and abs(j - target_j) <= 1:
        new_board = move_piece(board, tuple_from, tuple_to, piece_to_move, player)
        if new_board is None:
            return None

        if player == 0:
            new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "").replace("Q", "")
        else:
            new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "").replace("q", "")

        return new_board

    # check castles
    if player == 0 and tuple_from == (7, 4):
        if tuple_to == (7, 6) and board[61] == board[62] == "" and "K" in board[CASTLE_INDEX]:
            if check_on_multiple_squares([60, 61, 61], player, board):
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "K")
                new_board[63] = ""
                new_board[61] = "R"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "").replace("Q", "")
                return new_board
        elif tuple_to == (7, 2) and board[57] == board[58] == board[59] == "" and "Q" in board[CASTLE_INDEX]:
            if check_on_multiple_squares([60, 59, 58], player, board):
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "K")
                new_board[56] = ""
                new_board[59] = "R"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "").replace("Q", "")
                return new_board

    elif player == 1 and tuple_from == (0, 4):
        if tuple_to == (0, 6) and board[5] == board[6] == "" and "k" in board[CASTLE_INDEX]:
            if check_on_multiple_squares([4, 5, 6], player, board):
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "k")
                new_board[7] = ""
                new_board[5] = "r"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "").replace("q", "")
                return new_board
        elif tuple_to == (0, 2) and board[1] == board[2] == board[3] == "" and "q" in board[CASTLE_INDEX]:
            if check_on_multiple_squares([2, 3, 4], player, board):
                new_board = move_piece_without_checking_target(board, tuple_from, tuple_to, "k")
                new_board[0] = ""
                new_board[3] = "r"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "").replace("q", "")
                return new_board

    return None


def move_rook(tuple_from, tuple_to, player, board):
    i1, j1 = tuple_from
    i2, j2 = tuple_to

    piece_to_move = "R"
    if player == 1:
        piece_to_move = "r"

    if not (i1 == i2) == (j1 == j2):

        new_board = move_horizontal_or_vertical(tuple_from, tuple_to, player, board, piece_to_move)

        if new_board is not None:
            if player == 0:
                if tuple_from == (7, 0):
                    new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("Q", "")
                elif tuple_from == (7, 7):
                    new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "")
            elif player == 1:
                if tuple_from == (0, 0):
                    new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("q", "")
                elif tuple_from == (0, 7):
                    new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "")

        return new_board


def update_castling(tuple_from, tuple_to, player, board):
    if tuple_to == (7, 0) or tuple_from == (7, 0):
        board[CASTLE_INDEX] = board[CASTLE_INDEX].replace("Q", "")
    elif tuple_to == (7, 7) or tuple_from == (7, 7):
        board[CASTLE_INDEX] = board[CASTLE_INDEX].replace("K", "")

    if tuple_to == (0, 0) or tuple_from == (0, 0):
        board[CASTLE_INDEX] = board[CASTLE_INDEX].replace("q", "")
    elif tuple_to == (0, 7) or tuple_from == (0, 7):
        board[CASTLE_INDEX] = board[CASTLE_INDEX].replace("k", "")


def update_halfmove(piece_to_move, tuple_to, current_player, old_board, new_board):
    if piece_to_move != "P" and piece_to_player(old_board[tuple_to_index(tuple_to)], get_other_player(current_player)):
        new_board[HALFMOVE_CLOCK_INDEX] += 1
    else:
        new_board[HALFMOVE_CLOCK_INDEX] = 0


def move(tuple_from, tuple_to, board, promoting=None):
    if board[HALFMOVE_CLOCK_INDEX] == 101:
        return board

    if check_for_pawn_promotion(board) is not None:
        return board

    current_player = get_current_player(board)
    piece_to_move = board[tuple_to_index(tuple_from)]

    if not piece_to_player(piece_to_move, current_player):
        return board

    new_board = None
    # for comparison piece is cast to uppercase
    piece_to_move = piece_to_move.upper()

    if piece_to_move == "P":
        new_board = move_pawn(tuple_from, tuple_to, current_player, board)
    if piece_to_move == "N":
        new_board = move_knight(tuple_from, tuple_to, current_player, board)
    if piece_to_move == "B":
        new_board = move_bishop(tuple_from, tuple_to, current_player, board)
    if piece_to_move == "Q":
        new_board = move_queen(tuple_from, tuple_to, current_player, board)
    if piece_to_move == "K":
        new_board = move_king(tuple_from, tuple_to, current_player, board)
    if piece_to_move == "R":
        new_board = move_rook(tuple_from, tuple_to, current_player, board)

    # check if move was valid
    if new_board is None or check(new_board, current_player):
        return board
    else:
        update_halfmove(piece_to_move, tuple_to, current_player, board, new_board)
        update_castling(tuple_from, tuple_to, current_player, new_board)
        if promoting is not None:
            index = check_for_pawn_promotion(new_board)
            if index is not None:
                promote_pawn(index, promoting, new_board)

        change_player(new_board)
        return new_board


# only intended for UI uses, is too slow otherwise
def get_possible_moves_for_display(start_tuple, board):
    valid_moves = []

    # iterate over ever possible destination to find which moves are valid
    for i in range(8):
        for j in range(8):
            if not (i, j) == start_tuple:
                if not move(start_tuple, (i, j), board) == board:
                    valid_moves.append((i, j))
    return valid_moves


def update_board(move, old_board):
    new_board = move[3]
    piece_to_move = old_board[tuple_to_index(move[0])]
    tuple_from = move[0]
    tuple_to = move[1]
    current_player = get_current_player(old_board)

    update_halfmove(piece_to_move, tuple_to, current_player, old_board, new_board)
    update_castling(tuple_from, tuple_to, current_player, new_board)

    change_player(new_board)
    return move


def get_move_tuple(start_tuple, target_tuple, piece_to_move, starting_board, valid_moves, promoting=None):
    new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, piece_to_move)
    valid_moves.append((start_tuple, target_tuple, promoting, new_board))


def get_move_tuple_promoting(start_tuple, target_tuple, piece_to_move, starting_board, valid_moves):
    if piece_to_player(piece_to_move, 0):
        if target_tuple[0] == 0:
            for promoted_piece in WHITE_PIECES_PROMOTION:
                new_board = list.copy(starting_board)
                # make changes
                new_board[tuple_to_index(start_tuple)] = ""
                new_board[tuple_to_index(target_tuple)] = promoted_piece
                new_board[ENPASSANT_INDEX] = "-"
                valid_moves.append((start_tuple, target_tuple, promoted_piece, new_board))
        else:
            get_move_tuple(start_tuple, target_tuple, piece_to_move, starting_board, valid_moves)
    else:
        if target_tuple[0] == 7:
            for promoted_piece in BLACK_PIECES_PROMOTION:
                new_board = list.copy(starting_board)
                # make changes
                new_board[tuple_to_index(start_tuple)] = ""
                new_board[tuple_to_index(target_tuple)] = promoted_piece
                new_board[ENPASSANT_INDEX] = "-"
                valid_moves.append((start_tuple, target_tuple, promoted_piece, new_board))
        else:
            get_move_tuple(start_tuple, target_tuple, piece_to_move, starting_board, valid_moves)


def get_possible_moves_from_knight(start_tuple, valid_moves, piece, current_player, starting_board):
    for increment in KNIGHT_INCREMENTS:
        target_tuple = (start_tuple[0] + increment[0], start_tuple[1] + increment[1])
        target_index = tuple_to_index(target_tuple)
        if tuple_on_board(target_tuple) and not piece_to_player(starting_board[target_index], current_player):
            get_move_tuple(start_tuple, target_tuple, piece, starting_board, valid_moves)
    return valid_moves


def get_possible_moves_from_pawn(start_tuple, valid_moves, piece, current_player, starting_board):
    if current_player == 0:
        # check square in front of pawn
        target_tuple = (start_tuple[0] - 1, start_tuple[1])
        target_index = tuple_to_index(target_tuple)
        if tuple_on_board(target_tuple) and starting_board[target_index] == "":
            get_move_tuple_promoting(start_tuple, target_tuple, "P", starting_board, valid_moves)

            # check if pawn can move 2 forward
            target_tuple = (start_tuple[0] - 2, start_tuple[1])
            target_index = tuple_to_index(target_tuple)
            if tuple_on_board(target_tuple) and start_tuple[0] == 6 and starting_board[target_index] == "":
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "P")
                # r/anarchychess
                new_board[ENPASSANT_INDEX] = tuple_to_index((start_tuple[0] - 1, start_tuple[1]))
                valid_moves.append((start_tuple, target_tuple, None, new_board))

        # check infront to the left and right
        for target_tuple in [(start_tuple[0] - 1, start_tuple[1] - 1), (start_tuple[0] - 1, start_tuple[1] + 1)]:
            target_index = tuple_to_index(target_tuple)
            if tuple_on_board(target_tuple):
                if piece_to_player(starting_board[target_index], 1):
                    get_move_tuple_promoting(start_tuple, target_tuple, "P", starting_board, valid_moves)
                elif starting_board[ENPASSANT_INDEX] == tuple_to_index(target_tuple):
                    new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "P")
                    new_board[tuple_to_index((3, target_tuple[1]))] = ""
                    if target_tuple[0] == 0:
                        for possible_piece in WHITE_PIECES_PROMOTION:
                            valid_moves.append((start_tuple, target_tuple, possible_piece, new_board))
                    else:
                        valid_moves.append((start_tuple, target_tuple, None, new_board))


    else:
        # check square in front of pawn
        target_tuple = (start_tuple[0] + 1, start_tuple[1])
        target_index = tuple_to_index(target_tuple)
        if tuple_on_board(target_tuple) and starting_board[target_index] == "":
            get_move_tuple_promoting(start_tuple, target_tuple, "p", starting_board, valid_moves)

            # check if pawn can move 2 forward
            target_tuple = (start_tuple[0] + 2, start_tuple[1])
            target_index = tuple_to_index(target_tuple)
            if tuple_on_board(target_tuple) and start_tuple[0] == 1 and starting_board[target_index] == "":
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "p")
                # r/anarchychess
                new_board[ENPASSANT_INDEX] = tuple_to_index((start_tuple[0] + 1, start_tuple[1]))
                valid_moves.append((start_tuple, target_tuple, None, new_board))

        # check infront to the left and right
        for target_tuple in [(start_tuple[0] + 1, start_tuple[1] - 1), (start_tuple[0] + 1, start_tuple[1] + 1)]:
            target_index = tuple_to_index(target_tuple)
            if tuple_on_board(target_tuple):
                if piece_to_player(starting_board[target_index], 0):
                    get_move_tuple_promoting(start_tuple, target_tuple, "p", starting_board, valid_moves)
                elif starting_board[ENPASSANT_INDEX] == tuple_to_index(target_tuple):
                    new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "p")
                    new_board[tuple_to_index((3, target_tuple[1]))] = ""
                    if target_tuple[0] == 7:
                        for possible_piece in WHITE_PIECES_PROMOTION:
                            valid_moves.append((start_tuple, target_tuple, possible_piece, new_board))
                    else:
                        valid_moves.append((start_tuple, target_tuple, None, new_board))


def get_possible_moves_from_king(start_tuple, valid_moves, piece, current_player, starting_board):
    # check pieces right next to the king:
    increments = [-1, 0, 1]
    for i_increment in increments:
        for j_increment in increments:
            if i_increment == j_increment == 0:
                continue

            target_tuple = (start_tuple[0] + i_increment, start_tuple[1] + j_increment)
            target_index = tuple_to_index(target_tuple)
            if tuple_on_board(target_tuple) and not piece_to_player(starting_board[target_index], current_player):
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, piece)
                # update castling
                if current_player == 0:
                    new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "").replace("Q", "")
                else:
                    new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "").replace("q", "")

                valid_moves.append((start_tuple, target_tuple, None, new_board))
                # get_move_tuple(start_tuple, target_tuple, piece, starting_board, valid_moves)
    # check castling:

    if current_player == 0 and start_tuple == (7, 4):
        if starting_board[61] == starting_board[62] == "" and "K" in starting_board[CASTLE_INDEX]:
            if check_on_multiple_squares([60, 61, 61], current_player, starting_board):
                target_tuple = (7, 6)
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "K")
                new_board[63] = ""
                new_board[61] = "R"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "").replace("Q", "")
                valid_moves.append((start_tuple, target_tuple, None, new_board))
        if starting_board[57] == starting_board[58] == starting_board[59] == "" and "Q" in starting_board[CASTLE_INDEX]:
            if check_on_multiple_squares([60, 59, 58], current_player, starting_board):
                target_tuple = (7, 2)
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "K")
                new_board[56] = ""
                new_board[59] = "R"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("K", "").replace("Q", "")
                valid_moves.append((start_tuple, target_tuple, None, new_board))

    elif current_player == 1 and start_tuple == (0, 4):
        if starting_board[5] == starting_board[6] == "" and "k" in starting_board[CASTLE_INDEX]:
            if check_on_multiple_squares([4, 5, 6], current_player, starting_board):
                target_tuple = (0, 6)
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "k")
                new_board[7] = ""
                new_board[5] = "r"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "").replace("q", "")
                valid_moves.append((start_tuple, target_tuple, None, new_board))
        if starting_board[1] == starting_board[2] == starting_board[3] == "" and "q" in starting_board[CASTLE_INDEX]:
            if check_on_multiple_squares([2, 3, 4], current_player, starting_board):
                target_tuple = (0, 2)
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, "k")
                new_board[0] = ""
                new_board[3] = "r"
                new_board[CASTLE_INDEX] = new_board[CASTLE_INDEX].replace("k", "").replace("q", "")
                valid_moves.append((start_tuple, target_tuple, None, new_board))


def get_directional_possible_move(increments, start_tuple, valid_moves, piece, current_player, starting_board):
    for increment in increments:
        target_tuple = add_tuples(start_tuple, increment)
        while tuple_on_board(target_tuple) and starting_board[tuple_to_index(target_tuple)] == "":
            new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, piece)
            valid_moves.append((start_tuple, target_tuple, None, new_board))
            target_tuple = add_tuples(target_tuple, increment)

        if tuple_on_board(target_tuple):
            target_piece = starting_board[tuple_to_index(target_tuple)]
            if piece_to_player(target_piece, get_other_player(current_player)):
                new_board = move_piece_without_checking_target(starting_board, start_tuple, target_tuple, piece)
                valid_moves.append((start_tuple, target_tuple, None, new_board))


def get_possible_moves_horizontal_or_vertical(start_tuple, valid_moves, piece, current_player, starting_board):
    increments = [LEFT_INCREMENT, RIGHT_INCREMENT, UPWARD_INCREMENT, DOWNWARD_INCREMENT]
    get_directional_possible_move(increments, start_tuple, valid_moves, piece, current_player, starting_board)


def get_possible_moves_oblique(start_tuple, valid_moves, piece, current_player, starting_board):
    increments = [UP_LEFT_INCREMENT, UP_RIGHT_INCREMENT, DOWN_LEFT_INCREMENT, DOWN_RIGHT_INCREMENT]
    get_directional_possible_move(increments, start_tuple, valid_moves, piece, current_player, starting_board)


def get_possible_moves_from_rook(start_tuple, valid_moves, piece, current_player, starting_board):
    get_possible_moves_horizontal_or_vertical(start_tuple, valid_moves, piece, current_player, starting_board)


def get_possible_moves_from_bishop(start_tuple, valid_moves, piece, current_player, starting_board):
    get_possible_moves_oblique(start_tuple, valid_moves, piece, current_player, starting_board)


def get_possible_moves_from_queen(start_tuple, valid_moves, piece, current_player, starting_board):
    get_possible_moves_horizontal_or_vertical(start_tuple, valid_moves, piece, current_player, starting_board)
    get_possible_moves_oblique(start_tuple, valid_moves, piece, current_player, starting_board)


def get_possible_moves(start_tuple, starting_board):
    # move here is defined with: start, end, promotion and the board
    valid_moves = []
    current_player = get_current_player(starting_board)
    piece_to_move = starting_board[tuple_to_index(start_tuple)]

    # return empty list if the piece does not belong to the player
    if not piece_to_player(piece_to_move, current_player):
        return valid_moves

    # for comparison piece is cast to uppercase
    piece_to_move_upper = piece_to_move.upper()

    if piece_to_move_upper == "P":
        get_possible_moves_from_pawn(start_tuple, valid_moves, piece_to_move, current_player, starting_board)
    elif piece_to_move_upper == "N":
        get_possible_moves_from_knight(start_tuple, valid_moves, piece_to_move, current_player, starting_board)
    elif piece_to_move_upper == "B":
        get_possible_moves_from_bishop(start_tuple, valid_moves, piece_to_move, current_player, starting_board)
    elif piece_to_move_upper == "Q":
        get_possible_moves_from_queen(start_tuple, valid_moves, piece_to_move, current_player, starting_board)
    elif piece_to_move_upper == "K":
        get_possible_moves_from_king(start_tuple, valid_moves, piece_to_move, current_player, starting_board)
    elif piece_to_move_upper == "R":
        get_possible_moves_from_rook(start_tuple, valid_moves, piece_to_move, current_player,
                                     starting_board)

    # filter out moves, that create check and change player and update board
    valid_moves = [update_board(valid_move, starting_board) for valid_move in valid_moves if
                   not check(valid_move[3], current_player)]

    return valid_moves


def get_all_possible_moves(starting_board):
    all_moves = []
    for i in range(8):
        for j in range(8):
            for move in get_possible_moves((i, j), starting_board):
                all_moves.append(move)

    return all_moves


def get_winner_if_no_moves_available(board):
    current_player = get_current_player(board)
    if check(board, current_player):
        return get_other_player(current_player)
    else:
        return -1


def get_simple_rating(board):
    rating = 0
    for i in range(LAST_TILE_INDEX):
        rating += PIECE_VALUES.get(board[i])
    return rating
