import sys

import chess

MAX_VALUE = 576
GOAL_DEPTH = 3


def get_pos_from_move(move):
    return move[3]


def get_string_board_from_pos(pos):
    return ".".join(pos[
                    0:chess.LAST_TILE_INDEX + 1]) + f"{pos[chess.PLAYER_INDEX]}{pos[chess.CASTLE_INDEX]}{pos[chess.HALFMOVE_CLOCK_INDEX]}"


def get_string_board_from_move(move):
    return "".join(move[3][0:-1])


class Node:
    def __init__(self, position):
        self._is_sorted = False
        self._is_rated = False
        self.position = position
        self._rating = 0
        self._next_moves = None

    def get_next_moves(self):
        if self._next_moves is None:
            self._next_moves = chess.get_all_possible_moves(self.position)

        return self._next_moves

    def get_score(self):
        if not self._is_rated:
            possible_moves = self.get_next_moves()
            if len(possible_moves) == 0:
                result = chess.get_winner_if_no_moves_available(self.position)
                # draw
                if result == -1:
                    self._rating = 0
                # white wins
                elif result == 0:
                    self._rating = MAX_VALUE
                # black wins
                else:
                    self._rating = -MAX_VALUE
            else:
                self._rating = chess.get_simple_rating(self.position)
            self._is_rated = True

        return self._rating

    def sort_moves(self):
        if self._is_sorted:
            return


class MiniMaxTree:
    def __init__(self, ):
        self.previous_tree = {}
        self.current_tree = {}

    def get_node_from_pos(self, pos):
        board_key = get_string_board_from_pos(pos)
        # first check if we have the exact same board in current Tree
        node = self.current_tree.get(board_key)
        if node is None:
            # if we do not have the board in the current Tree, we check in the previous
            node = self.previous_tree.get(board_key)
            # if the node is still none, we make a new node
            if node is None:
                node = Node(pos)
                self.current_tree[board_key] = node

        return node

    def get_node_from_move(self, move):
        pos = get_pos_from_move(move)
        return self.get_node_from_pos(pos)

    def get_node_score(self, current_node, current_depth, goal_depth, current_player, alpha, beta):
        if goal_depth == current_depth:
            return current_node.get_score()

        next_player = chess.get_other_player(current_player)

        # if player is 0, we need higher score
        best_score = -MAX_VALUE - 1
        # if player is 1, we need a small score
        if current_player == 1:
            best_score = MAX_VALUE + 1

        for move in current_node.get_next_moves():
            node = self.get_node_from_move(move)
            if current_player == 0:
                score = self.get_node_score(node, current_depth + 1, goal_depth, next_player, alpha, beta)
                best_score = max(score, best_score)
                alpha = max(alpha, score)
                if alpha >= beta:
                    break
            else:
                score = self.get_node_score(node, current_depth + 1, goal_depth, next_player, alpha, beta)
                best_score = min(score, best_score)
                beta = min(beta, score)
                if alpha >= beta:
                    break

        return best_score

    def get_next_move(self, position):
        # prepare Tree
        self.previous_tree = self.current_tree
        self.current_tree = {}

        current_node = self.get_node_from_pos(position)
        current_player = chess.get_current_player(position)
        next_player = chess.get_other_player(current_player)

        best_move = None
        # if player is 0, we need higher score
        best_score = -MAX_VALUE - 1

        # if player is 1, we need a small score
        if current_player == 1:
            best_score = MAX_VALUE + 1

        alpha = -MAX_VALUE - 1
        beta = MAX_VALUE + 1
        for move in current_node.get_next_moves():
            node = self.get_node_from_move(move)
            if current_player == 0:
                score = self.get_node_score(node, 1, GOAL_DEPTH, next_player, alpha, beta)
                if score > best_score:
                    best_move = move
                    best_score = score
                alpha = max(alpha, score)
                if alpha >= beta:
                    break
            else:
                score = self.get_node_score(node, 1, GOAL_DEPTH, next_player, alpha, beta)
                if score < best_score:
                    best_move = move
                    best_score = score
                beta = min(beta, score)
                if alpha >= beta:
                    break

        return best_move


class Stockfish2:
    def __init__(self):
        self.mini_max_tree = MiniMaxTree()

    def make_next_move(self, position):
        return self.mini_max_tree.get_next_move(position)
